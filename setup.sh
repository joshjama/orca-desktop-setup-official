
#!/bin/bash
setup_configs() {

  mkdir -p $HOME/.local/share/orca-desktop-setup/orca-backup 2>&1 
  mkdir -p $HOME/.local/share/orca-desktop-setup/mate-desktop-backup 2>&1 
  cp -r $HOME/.local/share/orca/user-settings.conf $HOME/.local/share/orca-desktop-setup/orca-backup/ 2>&1 
  dconf dump /org/mate/ > $HOME/.local/share/orca-desktop-setup/mate-desktop-backup/mate-settings.conf 2>&1 
  dconf dump /org/mate/search-tool/ > ./mate-desktop/searchtool.conf 2>&1 
  cp -r ./orca/user-settings.conf $HOME/.local/share/orca/  2>&1
  dconf load /org/mate/ < ./mate-desktop/mate-settings.conf 2>&1 
  dconf load /org/mate/search-tool/ < ./mate-desktop/searchtool.conf 2>&1 
}

setup_configs 2>&1 
