#!/bin/bash

reset_orca_from_backup() {
  cp -r $HOME/.local/share/orca-desktop-setup/orca-backup/* $HOME/.local/share/orca/ 2>&1 
}

reset_orca_from_backup 2>&1 
